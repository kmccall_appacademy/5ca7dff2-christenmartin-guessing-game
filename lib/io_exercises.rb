# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  random_number = rand(1..100)
  tries = 1
  puts "Guess a number:"
  number = Integer(gets.chomp)
  until number == random_number
    puts "You guessed: #{number}."
    if number < random_number
      puts "too low"
    elsif number > random_number
      puts "too high"
    end
    tries += 1
    puts "Try again! Guess a number: "
    number = Integer(gets.chomp)
  end
  puts "You guessed it! The number was #{random_number}. It took you #{tries} tries to guess it."
end

def get_file
  puts "What file should I open?"
  file = gets.chomp
  File.open(file, "w") do
    contents = File.read(file)
    contents.split("").shuffle.join("")
    new_file = File.new(file + "-- shuffled.txt", "r")
    File.close
  end
end
